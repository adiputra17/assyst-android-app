package com.example.adiputra.assyst.Model;

/**
 * Created by adiputra on 11/25/2017.
 */

public class Token {

    int id;
    String nama, token;

    public Token(int id, String nama, String token) {
        this.id = id;
        this.nama = nama;
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
