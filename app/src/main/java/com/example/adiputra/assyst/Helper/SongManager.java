package com.example.adiputra.assyst.Helper;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.widget.ListView;

import com.example.adiputra.assyst.Model.Song;

import java.util.ArrayList;

/**
 * Created by Reaper on 11/28/2017.
 */

public class SongManager {

    private ArrayList<Song> songList = new ArrayList<Song>();
    private ListView songView;
    MediaPlayer mp = new MediaPlayer();
    Activity activity;

    public SongManager(Activity activity){
        this.activity = activity;
    }

    public void getSongList() {
        //retrieve song info
        ContentResolver musicResolver = activity.getContentResolver();
        Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);

        if(musicCursor!=null && musicCursor.moveToFirst()){
            //get columns
            int titleColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.TITLE);
            int idColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media._ID);
            int artistColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.ARTIST);
            //add songs to list
            do {
                long thisId = musicCursor.getLong(idColumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                Log.d("Music", thisTitle+":::"+musicUri.getEncodedPath());
                songList.add(new Song(thisId, thisTitle, thisArtist, musicUri));
            }
            while (musicCursor.moveToNext());
        }
    }
}
