package com.example.adiputra.assyst;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.adiputra.assyst.Activity.LoginActivity;
import com.example.adiputra.assyst.Model.Result;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ricoaw on 12/19/17.
 */

public class MediaHelper{

    Context context;

    public MediaHelper(Context context){
        this.context = context;
    }

    public void fromID(int ID, final OnDone onDone){
        MediaPlayer mediaPlayer = MediaPlayer.create(context, ID);
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                onDone.onDone("fromID");
            }
        });
    }

    public void fromURL(String url, final OnDone onDone){
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepare(); // might take long! (for buffering, etc)
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                onDone.onDone("fromURL");
            }
        });
    }
    public void SpeakOnline(final String message, final OnDone onDone){
        final String msg;
        if(message.length()>=100){
            msg = message.substring(0,99);
        }else{
            msg = message;
        }
        final String newMessage = msg.replace(" ","_")+".mp3";

        //GET HELIO_TOKEN
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        GsonBuilder gsonBuilder3 = new GsonBuilder();
        gsonBuilder3.setDateFormat("M/d/yy hh:mm a");
        final Gson gson = gsonBuilder3.create();
        String GETTOKEN_URL = "http://old.soundoftext.com/sounds";
        StringRequest stringRequest3 = new StringRequest(Request.Method.POST, GETTOKEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    SoundOfText s = gson.fromJson(response,SoundOfText.class);
                    if(s.isSuccess()){
                        String url = "http://old.soundoftext.com/static/sounds/id/"+newMessage;
                        fromURL(url,onDone);
                        onDone.onDone("SpeakOnline");
                    }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("","Error");
//                        Toast.makeText(LoginActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params = new HashMap<>();
                params.put("lang","id");
                params.put("text",msg);
                return params;
            }


            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }

        };
        requestQueue.add(stringRequest3);
    }


    public class SoundOfText{
        boolean success;

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }
    }
}
