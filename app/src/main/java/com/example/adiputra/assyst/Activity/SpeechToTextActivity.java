package com.example.adiputra.assyst.Activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.adiputra.assyst.Helper.SharedPref;
import com.example.adiputra.assyst.Helper.SpeakOut;
import com.example.adiputra.assyst.MediaHelper;
import com.example.adiputra.assyst.OnDone;
import com.example.adiputra.assyst.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SpeechToTextActivity extends Activity {

    private TextView txtSpeechInput;
    private ImageButton btnSpeak;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    SharedPref sharedPref;
    String sender;
    File cacheDir = new File(android.os.Environment.getExternalStorageDirectory(),"Assyst"), f;
    MediaPlayer audioOnline;

    String messages,status,mode,number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speech_to_text);

        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        sharedPref = new SharedPref(this);

        status = sharedPref.loadData("status");
        sender = sharedPref.loadData("sender");
        number = sharedPref.loadData("number");
        messages = sharedPref.loadData("kontak");
        Log.d("Exce",status+"\n"+sender+"\n"+messages);

        //First Tone
        MediaPlayer tone = MediaPlayer.create(this, R.raw.assyst_tone);
        tone.start();//Anda mendapatkan pesan dari


        //Panggilan
        if(status.equalsIgnoreCase("You got call from ")){
            mode = "call";
            messages = status;
            f = new File(android.os.Environment.getExternalStorageDirectory(),"Assyst/"+sender+".mp3");
            if(f.exists()) {
                Log.d("Exce", " Nomor ditemukan");
                new SpeakOut(this).fillTTS(messages);
                MediaHelper mediaHelper = new MediaHelper(getApplication());
                mediaHelper.fromURL(cacheDir.getPath() + "/" + sender + ".mp3", new OnDone() {
                    @Override
                    public void onDone(String response) {

                    }
                });
            }else{
                new SpeakOut(this).fillTTS(messages+sender);
            }
        }else{
        //SMS
            mode = "sms";
            MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.sms1);
            mediaPlayer.start();//Anda mendapatkan pesan dari
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    //If name exist
                    f = new File(android.os.Environment.getExternalStorageDirectory(),"Assyst/"+sender+".mp3");
                    if(f.exists()) {
                        Log.d("Exce", " Nomor ditemukan");
                        MediaHelper mediaHelper = new MediaHelper(getApplication());//Cari nama orang
                        mediaHelper.fromURL(cacheDir.getPath() + "/" + sender + ".mp3", new OnDone() {
                            @Override
                            public void onDone(String response) {
                                speakOnline(" yang berisi " + messages);
                            }
                        });
                    }else{
                        speakOnline(sender + " yang berisi " + messages);
                    }
                }
            });
        }

        txtSpeechInput = (TextView) findViewById(R.id.txtSpeechInput);
        txtSpeechInput.setText("");
        btnSpeak = (ImageButton) findViewById(R.id.btnSpeak);

        txtSpeechInput.setText(messages);

        // hide the action bar
        btnSpeak.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                promptSpeechInput();
            }
        });

        TelephonyManager mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if (mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }
        if (mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.ACTION_VOICE_SEARCH_HANDS_FREE, RecognizerIntent.EXTRA_PREFER_OFFLINE);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    txtSpeechInput.setText(result.get(0));

                    if(mode.equals("sms")&&result.get(0).toLowerCase().contains("ya")){
                        //Dapatkan jawaban apakah dia mengatakan ya
                        mode = "reply";
                        new MediaHelper(SpeechToTextActivity.this).fromID(R.raw.sms3, new OnDone() {
                            @Override
                            public void onDone(String response) {
                                promptSpeechInput();
                            }
                        });
                        Log.d("exce","sedang menjawab");
                    }else if(mode.equals("reply")){
                        //Kirim pesan
                        Log.d("exce","Sedang mengirim");
                        sendSMS(number,result.get(0).toLowerCase());
                    }else{
                        Log.d("exce","Sudah tiada");
                    }
                }
                break;
            }

        }
    }
    PhoneStateListener phoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            if (state == TelephonyManager.CALL_STATE_RINGING) {
                //Incoming call: Pause music
            } else if (state == TelephonyManager.CALL_STATE_IDLE) {
                //Not in call: Play music
            } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                //A call is dialing, active or on hold
            }
            super.onCallStateChanged(state, incomingNumber);
        }
    };

    public void speakOnline(String message){
        final String msg;
        if(message.length()>=100){
            msg = message.substring(0,99);
        }else{
            msg = message;
        }
        final String newMessage = msg.replace(" ","_")+".mp3";

        //GET HELIO_TOKEN
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        GsonBuilder gsonBuilder3 = new GsonBuilder();
        gsonBuilder3.setDateFormat("M/d/yy hh:mm a");
        final Gson gson = gsonBuilder3.create();
        String GETTOKEN_URL = "http://old.soundoftext.com/sounds";
        StringRequest stringRequest3 = new StringRequest(Request.Method.POST, GETTOKEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        MediaHelper.SoundOfText s = gson.fromJson(response,MediaHelper.SoundOfText.class);
                        if(s.isSuccess()){
                            String url = "http://old.soundoftext.com/static/sounds/id/"+newMessage;
                            audioFromURL(url);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("","Error");
//                        Toast.makeText(LoginActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params = new HashMap<>();
                params.put("lang","id");
                params.put("text",msg);
                return params;
            }


            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }

        };
        requestQueue.add(stringRequest3);
    }

    public void audioFromURL(String url){
        audioOnline = new MediaPlayer();
        audioOnline.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            audioOnline.setDataSource(url);
            audioOnline.prepare(); // might take long! (for buffering, etc)
            audioOnline.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        audioOnline.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer = MediaPlayer.create(SpeechToTextActivity.this, R.raw.sms2);
                mediaPlayer.start();
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        promptSpeechInput();
                    }
                });
            }
        });
    }

    void sendSMS(String phoneNo, String message){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + phoneNo));
        intent.putExtra("sms_body", message);
        startActivity(intent);
    }
}