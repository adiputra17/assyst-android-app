package com.example.adiputra.assyst.Helper;

import android.app.Activity;
import android.content.Context;

import android.media.AudioManager;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Reaper on 11/19/2017.
 */

public class SpeakOut extends Activity implements TextToSpeech.OnUtteranceCompletedListener{
    TextToSpeech tts;
    Context context;
    Activity activity;
    private final int REQ_CODE_SPEECH_INPUT = 100;

    public SpeakOut(Context context) {
        this.context = context;
    }

    public SpeakOut(Activity activity) {
        this.activity = activity;
    }

    public SpeakOut fillTTS(final String text) {
        final HashMap<String, String> myHashAlarm = new HashMap<String, String>();
        myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_RING));
        myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "SOME MESSAGE");
        if (context!=null) {//Activity
            tts = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {

                        int result = tts.setLanguage(Locale.US);

                        if (result == TextToSpeech.LANG_MISSING_DATA
                                || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                            Log.e("TTS", "This Language is not supported");
                        } else {
                            tts.speak(text, TextToSpeech.QUEUE_FLUSH, myHashAlarm);
                        }
                    } else {
                        Log.e("TTS", "Initilization Failed!");
                    }

                }
            });
        }else{//Context
            tts = new TextToSpeech(activity, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status == TextToSpeech.SUCCESS) {

                        int result = tts.setLanguage(Locale.US);

                        if (result == TextToSpeech.LANG_MISSING_DATA
                                || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                            Log.e("TTS", "This Language is not supported");
                        } else {
                            tts.speak(text, TextToSpeech.QUEUE_FLUSH, myHashAlarm);
                        }

                    } else {
                        Log.e("TTS", "Initilization Failed!");
                    }

                }
            });
        }
        tts.setOnUtteranceCompletedListener(this);
        CompletedCallbak(this);
        return this;
    }

    public TextToSpeech CompletedCallbak(TextToSpeech.OnUtteranceCompletedListener o){
        return tts;
    }

    @Override
    public void onUtteranceCompleted(String utteranceId) {
        Log.d("Speak", "Finished Utterance Implement");
        this.CompletedCallbak(new TextToSpeech.OnUtteranceCompletedListener() {
            @Override
            public void onUtteranceCompleted(String utteranceId) {

            }
        });
    }
}
