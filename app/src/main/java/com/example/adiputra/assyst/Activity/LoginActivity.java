package com.example.adiputra.assyst.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.adiputra.assyst.Helper.SharedPref;
import com.example.adiputra.assyst.Model.Result;
import com.example.adiputra.assyst.Model.User;
import com.example.adiputra.assyst.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class LoginActivity extends AppCompatActivity {
    SharedPref sharedPref = new SharedPref(this);
    public static final String mypreference = "mypref";
    public static final int Id = 0;
    public static final String Token = "tokenKey";
    private ProgressDialog progress;
    EditText etName;
    EditText etPass;
    private RequestQueue requestQueue, requestQueue2, requestQueue3;
    private Gson gson, gson2;
    private LinearLayout linearLayout;
    List<User> userList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        linearLayout = (LinearLayout) findViewById(R.id.linear_layout);

        String id = sharedPref.loadData("id");
        String token = sharedPref.loadData("token");

        //GET HELIO_TOKEN
        requestQueue3 = Volley.newRequestQueue(getApplicationContext());
        GsonBuilder gsonBuilder3 = new GsonBuilder();
        gsonBuilder3.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder3.create();
        String GETTOKEN_URL = "http://api.atrama-studio.com/backend/web/api-token?nama=helio_token&access-token="+token;
        StringRequest stringRequest3 = new StringRequest(Request.Method.GET, GETTOKEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Result result = gson.fromJson(response, Result.class);
                            Log.i(null,"response : "+response);
                            if(result.isResult()==true){
                                //Toast.makeText(LoginActivity.this, "helio_token : "+result.getTokenData().getToken(), Toast.LENGTH_SHORT).show();
                                sharedPref.saveData("helio_token", String.valueOf(result.getTokenData().getToken()));
                            }else{
                                Toast.makeText(LoginActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("","Error");
//                        Toast.makeText(LoginActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
        );
        requestQueue3.add(stringRequest3);

        requestQueue = Volley.newRequestQueue(LoginActivity.this);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();

        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        Button btnSignUp = (Button) findViewById(R.id.btnToSignUp);
        etPass = (EditText) findViewById(R.id.idPassword);
        etName = (EditText) findViewById(R.id.idUsername);

        if(!id.isEmpty() && !token.isEmpty()){
            //langsung masuk menu jika pernah login
            startActivity(new Intent(LoginActivity.this, Main2Activity.class));
            finish();
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (etName.getText().toString().equals("")) {
                    //Toast.makeText(LoginActivity.this, "Username cannot blank!", Toast.LENGTH_SHORT).show();
                    Snackbar.make(linearLayout,"Username cannot blank!",Snackbar.LENGTH_SHORT).show();
                } else if (etPass.getText().toString().equals("")) {
                    //Toast.makeText(LoginActivity.this, "Password cannot blank!", Toast.LENGTH_SHORT).show();
                    Snackbar.make(linearLayout,"Password cannot blank!",Snackbar.LENGTH_SHORT).show();
                } else {
                    progress = new ProgressDialog(LoginActivity.this);
                    progress.setMessage("Please wait...");
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setIndeterminate(true);
                    progress.setProgress(0);
                    progress.setCanceledOnTouchOutside(false);
                    progress.show();
                    cekLog(etName.getText().toString().trim(), etPass.getText().toString().trim());
                }
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });
    }

    public boolean cekLog(final String uname, final String pass) {
        String url = "http://api.atrama-studio.com/backend/web/auth/login";
        StringRequest req = new StringRequest(Request.Method.POST, url,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        final Result result = gson.fromJson(response, Result.class);
                        if(result.isResult()==true){
                            Toast.makeText(LoginActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                            sharedPref.saveData("id", String.valueOf(result.getUserData().getId()));
                            sharedPref.saveData("token", result.getUserData().getToken());
                            sharedPref.saveData("username", result.getUserData().getUsername());

                            //KIRIM EMAIL
                            requestQueue2 = Volley.newRequestQueue(LoginActivity.this);
                            String compose_url = "https://api.mainapi.net/helio/v1.0/composemobile";
                            StringRequest req2 = new StringRequest(Request.Method.POST, compose_url,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            try {
                                                Result result = gson.fromJson(response, Result.class);
                                                Log.i("","Email berhasil dikirim");
                                                //PINDAH ACTIVITY KE MAIN
//                            startActivity(new Intent(LoginActivity.this, ListActivity.class));
//
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.e("", "Gagal kirim email");
                                            //Toast.makeText(LoginActivity.this, "Helio Credentials", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                            )
                            {
                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    Map<String, String> headers = new HashMap<String, String>();
                                    headers.put("Authorization", "Bearer " +String.valueOf(sharedPref.loadData("helio_token")));
                                    headers.put("Accept", "application/json");
                                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                                    return headers;
                                }

                                protected Map<String,String> getParams(){
                                    Map<String,String> params = new HashMap<String, String>();
                                    params.put("token", "TFLjnfEtKJWPSqGz_EscxG8fL-o6MTUxMTY3MTgxNDgyMzg5Nzc1Nw==");
                                    params.put("to", result.getUserData().getEmail());
                                    params.put("subject", "Assyst Login Success");
                                    Calendar c = Calendar.getInstance();
                                    params.put("body", "Login from "+ Build.MANUFACTURER +" - "+Build.MODEL+"\n At : "+c.getTime());
                                    return params;
                                }

                            };
                            requestQueue2.add(req2);

                            //PINDAH ACTIVITY KE MAIN
//                            startActivity(new Intent(LoginActivity.this, ListActivity.class));
                            startActivity(new Intent(LoginActivity.this, Main2Activity.class));
                            finish();
                            progress.hide();
                        }else{
                            progress.hide();
                            Snackbar.make(linearLayout,result.getMessage(),Snackbar.LENGTH_SHORT).show();
                            //Toast.makeText(LoginActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Get Data : ", error.toString());
                    Toast.makeText(LoginActivity.this, "Check Internet Connection!", Toast.LENGTH_SHORT).show();
                }
            }
        )
        {
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", uname.toString().trim());
                params.put("password", pass.toString().trim());
                return params;
            }

        };
        requestQueue.add(req);
        return true;
    }
}
