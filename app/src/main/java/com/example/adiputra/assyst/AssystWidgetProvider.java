package com.example.adiputra.assyst;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.adiputra.assyst.Activity.ListVoucherActivity;
import com.example.adiputra.assyst.Activity.Main2Activity;
import com.example.adiputra.assyst.Activity.MapActivity;
import com.example.adiputra.assyst.Activity.SaveLocationActivity;
import com.example.adiputra.assyst.Activity.SettingsActivity;
import com.example.adiputra.assyst.Helper.SharedPref;
import com.example.adiputra.assyst.Model.Result;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Reaper on 12/5/2017.
 */

public class AssystWidgetProvider extends AppWidgetProvider {

    private RequestQueue requestQueue;
    private Gson gson;

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int N = appWidgetIds.length;

        // Perform this loop procedure for each App Widget that belongs to this provider
        for (int i = 0; i < N; i++) {
            int appWidgetId = appWidgetIds[i];

            // Create an Intent to launch ExampleActivity
            Intent i1 = new Intent(context, MapActivity.class);
            PendingIntent pendingIntent1 = PendingIntent.getActivity(context, 0, i1, 0);
            Intent i2 = new Intent(context, SettingsActivity.class);
            PendingIntent pendingIntent2 = PendingIntent.getActivity(context, 0, i2, 0);
            Intent i3 = new Intent(context, ListVoucherActivity.class);
            PendingIntent pendingIntent3 = PendingIntent.getActivity(context, 0, i3, 0);

            // Get the layout for the App Widget and attach an on-click listener
            // to the button
            final RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.assyst_widget);
            views.setOnClickPendingIntent(R.id.iv_configure, pendingIntent1);
            views.setOnClickPendingIntent(R.id.iv_setting, pendingIntent2);
            views.setOnClickPendingIntent(R.id.iv_voucher, pendingIntent3);
            views.setTextViewText(R.id.tv_count_configure, "Your Personal Assystant");

            //GET COUNT CONFIGURE
            //GET POINT
            final SharedPref sharedPref = new SharedPref(context.getApplicationContext());
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setDateFormat("M/d/yy hh:mm a");
            gson = gsonBuilder.create();
            String id = sharedPref.loadData("id");
            String token = sharedPref.loadData("token");
            String COUNT_URL = "http://api.atrama-studio.com/backend/web/api-configure/my-count?user_id="+id+"&access-token="+token;
            StringRequest stringRequest = new StringRequest(Request.Method.POST, COUNT_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                Result result = gson.fromJson(response, Result.class);
                                Log.i(null,"response : "+response);
                                if(result.isResult()==true){
                                    //String count = result.getConfigureData().;
                                    //views.setTextViewText(R.id.tv_count_configure, "123");
                                    //Toast.makeText(Main2Activity.this, "Your Point : "+result.getPointData().getTotal(), Toast.LENGTH_SHORT).show();
                                }else{
                                    //Toast.makeText(Main2Activity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Toast.makeText(AssystWidgetProvider.this.getClass(),error.toString(),Toast.LENGTH_LONG).show();
                        }
                    }
            );
            requestQueue.add(stringRequest);
            //http://api.atrama-studio.com/backend/web/api-configure/my-count?user_id=36&access-token=0CzacU6cikbdOwIypPaQXRBTQbSqLpJo

            // Tell the AppWidgetManager to perform an update on the current app widget
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }
}
