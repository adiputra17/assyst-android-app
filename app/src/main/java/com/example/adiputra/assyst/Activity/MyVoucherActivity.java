package com.example.adiputra.assyst.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.adiputra.assyst.Adapter.ListMyVoucherAdapter;
import com.example.adiputra.assyst.Adapter.ListVoucherAdapter;
import com.example.adiputra.assyst.Helper.SharedPref;
import com.example.adiputra.assyst.Model.Result;
import com.example.adiputra.assyst.Model.Voucher;
import com.example.adiputra.assyst.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyVoucherActivity extends AppCompatActivity {

    private TextView tvPoint;
    private List<Voucher> voucherList = new ArrayList<>();
    private List<Voucher> tempVoucherList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ListMyVoucherAdapter vAdapter;
    Context context;
    //parse json
    private RequestQueue requestQueue;
    private Gson gson;
    SharedPref sharedPref = new SharedPref(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_voucher);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("My Voucher");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //onBackPressed();
                onBackPressed();
                finish();
            }
        });

        tvPoint = (TextView) findViewById(R.id.tv_main_point);
        tvPoint.setText(sharedPref.loadData("point"));

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        // TODO: 1/14/2018 LIST MY VOUCHER
        vAdapter = new ListMyVoucherAdapter(voucherList, MyVoucherActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(vAdapter);

        requestQueue = Volley.newRequestQueue(getApplicationContext());
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();
        String id = sharedPref.loadData("id");
        String token = sharedPref.loadData("token");
        //Toast.makeText(ListVoucherActivity.this, "id : "+id+"\ntoken : "+token, Toast.LENGTH_SHORT).show();
        String GETVOUCHER = "http://api.atrama-studio.com/backend/web/api-voucher/my-voucher?user_id="+id+"&access-token="+token;
        StringRequest req = new StringRequest(Request.Method.POST, GETVOUCHER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            Result result = gson.fromJson(response, Result.class);
                            Log.i("Response : ", response);
                            Log.i("Response : ", String.valueOf(result.isResult()));
                            if(result.isResult()==true){
                                Toast.makeText(MyVoucherActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                                JsonParser jsonParser = new JsonParser();
                                JsonObject jo = (JsonObject)jsonParser.parse(response);
                                JsonArray jsonArr = jo.getAsJsonArray("voucherData");
                                List<Voucher> vouchers = Arrays.asList(gson.fromJson(jsonArr, Voucher[].class));
                                for (Voucher post : vouchers) {
                                    voucherList.add(new Voucher(
                                            post.getId(),
                                            post.getProduk_id(),
                                            post.getNama_voucher(),
                                            post.getKategori_voucher(),
                                            post.getDeskripsi(),
                                            post.getHarga(),
                                            post.getMasa_tenggang()
                                    ));
                                }
                                vAdapter.notifyDataSetChanged();
                            }else{
                                Toast.makeText(MyVoucherActivity.this, ""+result.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Get Data : ", error.toString());
                        Toast.makeText(MyVoucherActivity.this, "Check Internet Connection!", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        requestQueue.add(req);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            Intent intent = new Intent(MyVoucherActivity.this, ListVoucherActivity.class);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return true;
        }
        return false;
    }
}
