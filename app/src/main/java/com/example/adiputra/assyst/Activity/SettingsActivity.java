package com.example.adiputra.assyst.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adiputra.assyst.Helper.SharedPref;
import com.example.adiputra.assyst.R;

public class SettingsActivity extends AppCompatActivity {

    SharedPref sharedPref = new SharedPref(this);
    private Button btnLogout, btnAbout, btnChangeName, btnChangeRingtone;
    private TextView tvAssystName, tvPathRingtone, tvVolume;
    private CardView cvRingtone, cvAbout, cvLogout, cvSim;
    private SeekBar sbVolume;
    final static int RQS_OPEN_AUDIO_MP3 = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbarMap = (Toolbar) findViewById(R.id.toolbarMap);
        toolbarMap.setTitle("Settings");
        setSupportActionBar(toolbarMap);
        toolbarMap.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbarMap.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        cvSim = (CardView) findViewById(R.id.cv_sim);
        cvSim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.setAction(Settings.ACTION_NETWORK_OPERATOR_SETTINGS);
                startActivity(i);
            }
        });

        final AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        sbVolume = (SeekBar) findViewById(R.id.sb_volume);
        sbVolume.setProgress(audio.getStreamVolume(AudioManager.STREAM_RING));
        sbVolume.setMax(audio.getStreamMaxVolume(AudioManager.STREAM_RING));
        sbVolume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                audio.setStreamVolume(AudioManager.STREAM_RING|AudioManager.STREAM_NOTIFICATION,progress,0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        tvPathRingtone = (TextView) findViewById(R.id.tv_path_ringtone);
        if(!sharedPref.loadData("path_ringtone").isEmpty()){
            tvPathRingtone.setText(sharedPref.loadData("path_ringtone"));
        }

        cvRingtone = (CardView) findViewById(R.id.cv_ringtone);
        cvRingtone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setType("audio/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(i, "Open Audio (mp3) file"), RQS_OPEN_AUDIO_MP3);
            }
        });



        cvAbout = (CardView) findViewById(R.id.cv_about);
        cvAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SettingsActivity.this, AboutActivity.class);
                startActivity(i);
            }
        });

        cvLogout = (CardView) findViewById(R.id.cv_logout);
        cvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(v.getContext());
                alertDialogBuilder.setMessage("Are you sure ?");
                alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        sharedPref.saveData("id", "");
                        sharedPref.saveData("token", "");
                        Intent i = new Intent(SettingsActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    }
                });

                alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK){
            if(requestCode == RQS_OPEN_AUDIO_MP3){
                Uri audioFileUri = data.getData();
                tvPathRingtone.setText(audioFileUri.getPath());
                sharedPref.saveData("path_ringtone", audioFileUri.getPath());
            }
        }
    }
}
