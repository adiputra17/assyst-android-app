package com.example.adiputra.assyst.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.PRDownloader;
import com.example.adiputra.assyst.Helper.ContactReader;
import com.example.adiputra.assyst.Helper.SharedPref;
import com.example.adiputra.assyst.MediaHelper;
import com.example.adiputra.assyst.R;
import com.example.adiputra.assyst.Service.MapIntentService;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.DrawableBanner;
import ss.com.bannerslider.views.BannerSlider;

public class Main2Activity extends AppCompatActivity {
    SharedPref sharedPref = new SharedPref(this);
    MapView mMapView;
    private GoogleMap googleMap;
    private ImageButton ibConfigure, ibSetting, ibVoucher;
    List<ContactReader.Contact> contacts = new ArrayList<>();
    RequestQueue requestQueue;
    GsonBuilder gsonBuilder3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        requestQueue = Volley.newRequestQueue(this);
        gsonBuilder3 = new GsonBuilder();

        Intent service = new Intent(Main2Activity.this, MapIntentService.class);
        startService(service);

        String username = sharedPref.loadData("username");
        Toolbar toolbarMap = (Toolbar) findViewById(R.id.toolbarMap);
        toolbarMap.setTitle("Hi, "+username+"!");
        setSupportActionBar(toolbarMap);
        //toolbarMap.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbarMap.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        //SLIDER
        BannerSlider bannerSlider = (BannerSlider) findViewById(R.id.slider);
        List<Banner> banners=new ArrayList<>();

        //SLIDER
        banners.add(new DrawableBanner(R.drawable.assyst12));
        banners.add(new DrawableBanner(R.drawable.assyst22));
        banners.add(new DrawableBanner(R.drawable.assyst32));
        bannerSlider.setBanners(banners);

        //BUTTON
        ibConfigure = (ImageButton) findViewById(R.id.ib_configure);
        ibConfigure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main2Activity.this, ListActivity.class);
                startActivity(i);
            }
        });
        ibSetting = (ImageButton) findViewById(R.id.ib_setting);
        ibSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main2Activity.this, SettingsActivity.class);
                startActivity(i);
            }
        });
        ibVoucher = (ImageButton) findViewById(R.id.ib_voucher);
        ibVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main2Activity.this, ListVoucherActivity.class);
                startActivity(i);
            }
        });

        //MAP
        mMapView = (MapView) findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {

            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    googleMap.setMyLocationEnabled(true);
                    return;
                } else {
                    if (!googleMap.isMyLocationEnabled())
                        googleMap.setMyLocationEnabled(true);
                    LocationManager lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                    Location myLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (myLocation == null) {
                        Criteria criteria = new Criteria();
                        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                        String provider = lm.getBestProvider(criteria, true);
                        myLocation = lm.getLastKnownLocation(provider);
                    }
                    if (myLocation != null) {
//                        sharedPref.saveData("set_latitude", String.valueOf(myLocation.getLatitude()));
//                        sharedPref.saveData("set_longitude", String.valueOf(myLocation.getLongitude()));
                        LatLng userLocation = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()), 15));
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 15), 1500, null);
                    }
                }
            }
        });

        //Export all contact into audio mp3
        ContactReader contactReader = new ContactReader(this);
        contacts = contactReader.listContact();
        contacts = onceOnly();

        checkContact();
    }

    public void checkContact(){
        File cacheDir;
        for(ContactReader.Contact c : contacts){
            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"Assyst/"+c.getName()+".mp3");
            if(!cacheDir.exists()) {
                postMessage(c.getName());
            }
        }
    }

    public void postMessage(final String msg){
        final String newMessage = msg.replace(" ","_")+".mp3";

        //GET HELIO_TOKEN
        gsonBuilder3.setDateFormat("M/d/yy hh:mm a");
        final Gson gson = gsonBuilder3.create();
        String GETTOKEN_URL = "http://old.soundoftext.com/sounds";
        StringRequest stringRequest3 = new StringRequest(Request.Method.POST, GETTOKEN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("Exce", " Post complete");
                        MediaHelper.SoundOfText s = gson.fromJson(response,MediaHelper.SoundOfText.class);
                        if(s.isSuccess()){
                            String url = "http://old.soundoftext.com/static/sounds/id/"+newMessage;
                            download(url, msg);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("","Error");
//                        Toast.makeText(LoginActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params = new HashMap<>();
                params.put("lang","id");
                params.put("text",msg);
                return params;
            }


            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }

        };
        requestQueue.add(stringRequest3);

    }

    public List<ContactReader.Contact> onceOnly() {
        Log.d("Exce", "Mulai kontak");
        List<ContactReader.Contact> newContact = new ArrayList<>();

        newContact.add(contacts.get(0));//Masukan kontak pertama
        boolean same = false;
        for (ContactReader.Contact c : contacts){//Untuk masing masing kontak
            for (ContactReader.Contact c2 : newContact) {//tampilkan semua kontak yang baru
                if(c.getName().equalsIgnoreCase(c2.getName())){
                    same = true;
                    break;
                }
            }
            if(!same)
                newContact.add(c);
            same = false;
        }

        return newContact;
    }

    void download(String url, final String title){
        File cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"Assyst");
        if(!cacheDir.exists()) {
            cacheDir.mkdirs();
        }

        final String filepath = cacheDir.getPath();

        int download = PRDownloader.download(url,filepath,title+".mp3").build().
                start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        Log.d("Exce", title+" downloaded");
                    }
                    @Override
                    public void onError(Error error) {
                        Log.d("Exce", title +" Download failed");
                    }
                });
    }
}
