package com.example.adiputra.assyst.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.example.adiputra.assyst.Helper.SharedPref;
import com.example.adiputra.assyst.Model.Voucher;
import com.example.adiputra.assyst.R;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by adiputra on 1/14/2018.
 */

public class ListMyVoucherAdapter extends RecyclerView.Adapter<ListMyVoucherAdapter.MyViewHolder> {

    Context context;
    private RequestQueue requestQueue;
    private RequestQueue requestQueue2;
    private Gson gson;
    private List<Voucher> voucherList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nama_voucher, deskripsi_voucher, harga_voucher, masa_tenggang_voucher;
        public RelativeLayout relativeLayout;
        public CardView cardViewVoucher;

        public MyViewHolder(View view) {
            super(view);
            cardViewVoucher = (CardView) view.findViewById(R.id.cv_voucher_item);
            nama_voucher = (TextView) view.findViewById(R.id.tv_nama_voucher);
            deskripsi_voucher = (TextView) view.findViewById(R.id.tv_desc_voucher);
            harga_voucher = (TextView) view.findViewById(R.id.tv_harga);
            masa_tenggang_voucher = (TextView) view.findViewById(R.id.tv_masa_tenggang);
        }
    }

    public ListMyVoucherAdapter(List<Voucher> voucherList, Context context) {
        super();
        this.voucherList = voucherList;
        this.context = context;
    }

    @Override
    public ListMyVoucherAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_voucher, parent, false);

        return new ListMyVoucherAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ListMyVoucherAdapter.MyViewHolder holder, int position) {
        final SharedPref sharedPref = new SharedPref(context);
        final Voucher voucher = voucherList.get(position);
        holder.nama_voucher.setText(voucher.getNama_voucher().toUpperCase());
        holder.deskripsi_voucher.setText(voucher.getDeskripsi());
        holder.harga_voucher.setText(voucher.getHarga()+" P");
        holder.masa_tenggang_voucher.setText("Expired : "+voucher.getMasa_tenggang());
    }

    @Override
    public int getItemCount() {
        return voucherList.size();
    }
}
