package com.example.adiputra.assyst;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.example.adiputra.assyst.Activity.SpeechToTextActivity;
import com.example.adiputra.assyst.Helper.ContactReader;
import com.example.adiputra.assyst.Helper.SharedPref;

/**
 * Created by rickReaper on 6/15/2017.
 */

public class AssystBroadcast extends BroadcastReceiver {
    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static final String PHONE_RECEIVED = "android.intent.action.NEW_OUTGOING_CALL";
    private static final String TAG = "SMSBroadcastReceiver";
    Context coni;
    SharedPref sharedPref;

    @Override
    public void onReceive(Context context, Intent intent) {
        sharedPref = new SharedPref(context);
        Log.i(TAG, "Intent Recieved: " + intent.getAction());
        coni = context;
        if (intent.getAction().equals(SMS_RECEIVED)) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdus = (Object[]) bundle.get("pdus");
                final SmsMessage[] messages = new SmsMessage[pdus.length];
                for (int i = 0; i < pdus.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                }
                if (messages.length > -1) {
                    Log.d("Pesan", messages[0].getMessageBody());
                    Intent i = new Intent(context, SpeechToTextActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    sharedPref.saveData("status", "You got message from ");//Message Body
                    sharedPref.saveData("sender", messages[0].getOriginatingAddress());//Message Body
                    sharedPref.saveData("number", messages[0].getMessageBody());//Message Body

                    String name = new ContactReader(context).findByPhone(messages[0].getMessageBody()).getName();
                    sharedPref.saveData("kontak", name);//Message Body

                    context.startActivity(i);
                }
            }
        //PHONE RECEIVED CONDITION
        }else if(intent.getAction().equals(PHONE_RECEIVED)){
            Log.d("Panggilan", intent.getExtras().getString("android.intent.extra.PHONE_NUMBER"));
        }else{
            String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
            String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            sharedPref.saveData("status", "You got call from ");//Message Body
            sharedPref.saveData("kontak", number);//Message Body
            ContactReader.Contact contact =  new ContactReader(context).findByPhone(number);
            Log.d("Panggilan", stateStr + " from "+contact.getName()+ "|||" + number);
            sharedPref.saveData("sender", contact.getName());//Message Body
            final String LOG_TAG = "TelephonyAnswer";

            TelephonyManager tm = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);

            try {
                Log.d("Call", "Try to answer call automaticly");
                if (tm == null) {
                    // this will be easier for debugging later on
                    throw new NullPointerException("tm == null");
                }

                // do reflection magic
                tm.getClass().getMethod("answerRingingCall").invoke(tm);
            } catch (Exception e) {

                // TODO decide how to handle this state
                // you probably want to set some failure state/go to fallback
                Log.d("Call", "Error");
            }
            Intent i = new Intent(context, SpeechToTextActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
            int state = 0;
            if(stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)){
                state = TelephonyManager.CALL_STATE_IDLE;
            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
                state = TelephonyManager.CALL_STATE_OFFHOOK;
            }
            else if(stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)){
                state = TelephonyManager.CALL_STATE_RINGING;
            }
        }
    }
}