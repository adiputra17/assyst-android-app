package com.example.adiputra.assyst;

/**
 * Created by ricoaw on 12/19/17.
 */

public interface OnDone{
    void onDone(String response);
}